**/.terraform/*		игнорируем все файлы, которые находятся в поддиректории .terraform\
.tfstate		игнорируем файлы c расширением .tfstate\
crash.log		игнорируем лог-файл crash.log\
.tfvars			игнорируем файлы c расширением .tfvars\
override.tf		игнорируем файл override.tf\
override.tf.json	игнорируем файл override.tf.json\
*_override.tf		игнорируем файл, который заканчивается на _override.tf\
*_override.tf.json	игнорируем файл, который заканчивается на _override.tf.json\
.terraformrc		игнорируем скрытый файл .terraformrc\
terraform.rc		игнорируем файл terraform.rc
